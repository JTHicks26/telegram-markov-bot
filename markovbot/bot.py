from telegram.ext import Updater, CommandHandler, CallbackContext, Filters, MessageHandler
from telegram import Update

import requests
import re
import random
import pickle
import signal
import sys

# This should take care of installing every dependency except for Tesseract.
# pip install --user python-telegram-bot pytesseract google_trans_new argparse syllables opencv-python-headless

# Python requirements for OCR.
# Also requires OpenCV (headless), Leptonica, and Tesseract on the system.
# Tesseract must be installed to PATH.
try:
    from PIL import Image
    import pytesseract
    import argparse
    import cv2
    import os
except:
    print("Could not load libraries necessary for OCR.")

# Python requirements for translation.
try:
    from google_trans_new import google_translator  
    translation_enabled = True
except:
    print("Could not load libraries necessary for translation.")
    translation_enabled = False
    
# Python requirement for /markovhaiku.
try:
    import syllables
except:
    print("Could not load libraries necessary for /markovhaiku.")

class Bot:
    def __init__(self, config):
        self.api_key = config.api_key
        self.rand_response_freq = config.rand_response_freq
        self.message_max_words = config.message_max_words
        self.essay_min_words = config.essay_min_words
        self.markov_cache_file = config.markov_cache_file
        self.markov_ocr_enabled = config.markov_ocr_enabled
        self.markov_print_ocr =  config.markov_print_ocr
        self.markov_image_file = config.markov_image_file
        self.markov_image_file_grayscale = config.markov_image_file_grayscale
        self.markov_data = {}
    
    def run(self):
        # load data if it exists
        try:
            with open(self.markov_cache_file, 'rb') as f:
                self.markov_data = pickle.load(f)
            print("Loaded cache")
        except:
            print("No cache stored")

        updater = Updater(self.api_key, use_context=True)
        dp = updater.dispatcher
        dp.add_handler(CommandHandler('markov', self.markov))
        dp.add_handler(CommandHandler('markovessay', self.markov_essay))
        dp.add_handler(CommandHandler('markovhaiku', self.markov_haiku))
        dp.add_handler(CommandHandler('markovclear', self.markov_clear))
        dp.add_handler(CommandHandler('markovsave', self.save_cmd))
        dp.add_handler(MessageHandler(Filters.text, self.capture_msg))
        dp.add_handler(MessageHandler(Filters.sticker, self.capture_sticker))
        if (self.markov_ocr_enabled):
            dp.add_handler(MessageHandler(Filters.photo, self.capture_image))
        self.updater = updater
        updater.start_polling()
        updater.idle()
        self.save()

    def save(self):
        try:
            with open(self.markov_cache_file, 'wb') as f:
                pickle.dump(self.markov_data, f, pickle.HIGHEST_PROTOCOL)
            print("Saved cache")
        except:
            print("Failed to save cache")

    def save_cmd(self, update: Update, context: CallbackContext):
        self.save()

    def markov_msg(self, chat_id):
        print("Generating markov chain")

        if chat_id not in self.markov_data:
            self.markov_data[chat_id] = {}
        mk_data = self.markov_data[chat_id]

        words = []
        cur_word = 'CHAIN_START'
        if 'CHAIN_START' not in mk_data:
            return

        while len(words) <= self.message_max_words:
            if cur_word not in mk_data:
                break
            
            freqs = mk_data[cur_word]
            total = 0
            for i in freqs:
                total += i[1]

            # pick random word
            select = random.randint(1,total)
            accumulator = 0

            for i in range(0, len(freqs)):
                if accumulator < select and accumulator + freqs[i][1] >= select:
                    cur_word = freqs[i][0]
                    break
                else:
                    accumulator += freqs[i][1]

            if cur_word == 'CHAIN_END':
                break
            else:
                words.append(cur_word)

        text = ' '.join(words)
        return text
    
    def markov(self, update: Update, context: CallbackContext):
        chat_id = update.message.chat_id
        text = self.markov_msg(chat_id)

        if not text:
            context.bot.send_message(chat_id=chat_id, text="No Data!")
        elif text.startswith("STICKER:"):
            text = text[len("STICKER:"):]
            context.bot.send_sticker(chat_id=chat_id, sticker=text)
        else:
            context.bot.send_message(chat_id=chat_id, text=text)
            
    def markov_essay(self, update: Update, context: CallbackContext):
        print("\nGenerating markov essay...")
        chat_id = update.message.chat_id
        text = self.markov_msg(chat_id)
        if not text:
            context.bot.send_message(chat_id=chat_id, text="No Data!")
        
        while len(text.split(" ")) <= self.essay_min_words:
            newtext = self.markov_msg(chat_id)
            if not newtext.startswith("STICKER:"):
                text += " " + newtext
    
        print("Markov essay complete.\n")
        context.bot.send_message(chat_id=chat_id, text=text)
        
    def markov_haiku(self, update: Update, context: CallbackContext):
        print("\nGenerating markov haiku...")
        chat_id = update.message.chat_id
        
        haiku_array = []
        text_word_array = []
        syllables_wanted = 5            #This goes from 5 to 7 to 5
        while len(haiku_array) != 3:    #While haiku does not have 3 lines
            text = self.markov_msg(chat_id).replace("\n", " ")      #Generate a markov chain, remove lines breaks
            text = ''.join([i for i in text if not i.isdigit()])    #Remove all numbers
            text = re.sub('[@#%^&*()_=+;:\"<>`~\|]+', '', text)     #Remove most special characters
            text_word_array = text.split(" ")                       #Create text_word_array with each element being one word
            
            while ( text.find("STICKER:") != -1 ) or ( syllables.estimate(text) < syllables_wanted ): #Make sure no stickers, or too-low-syllable text are run through the function
                text = self.markov_msg(chat_id).replace("\n", " ")
                text = ''.join([i for i in text if not i.isdigit()])
                text = re.sub('[@#%^&*()_=+;:\"<>`~\|]+', '', text)
                text_word_array = text.split(" ")
                
            begindex = 0    #begindex = beginning index                 huehue
            endex = 0       #endex = end endex                          huehue        
            totalstring = ""
            line_added = False
            
            #These nested while loops function similar to a 2d array, but in one dimension
            while ( begindex < len(text_word_array) ) and ( len(haiku_array) != 3 ):    #While begindex is not out-of-bounds, and haiku_array is not complete
                if (line_added):                        #If a haiku line has been found in this markov message, break loop to generate a new markov message
                    line_added = False                      #May be able to replace this with "begindex = endex" and remove break statement. Would prevent generating new markov messages, but potentially less variability in haiku lines.
                    break
                endex = begindex + 1                    #Set end index one word after beginning index
                totalstring = text_word_array[begindex] #totalstring is set to the first word in the markov message
                while ( endex < len(text_word_array) ) and ( syllables.estimate(totalstring.lower()) < syllables_wanted ) and ( len(haiku_array) != 3 ):    #While endex is not out-of-bounds, and totalstring does not have enough syllables, and haiku_array is not complete
                    totalstring += " " + text_word_array[endex]     #Add the next word in the message to totalstring
                    endex +=1                                       #Move end index to the next word
                    if syllables.estimate(totalstring.lower()) == syllables_wanted:     #If totalstring is the correct number of syllables long. Must be lowercase for syllables library to work right ¯\_(ツ)_/¯
                        haiku_array.append(totalstring)                                     #Add totalstring as a haiku line
                        
                        if syllables_wanted == 5:   #Toggle to the wanted number of syllables for next loop
                            syllables_wanted = 7
                        elif syllables_wanted == 7:
                            syllables_wanted = 5
                        line_added = True
                        break
                    endex +=1   #Move end index to the next word and loop
                begindex +=1    #Move beginning cursor to the next word and loop
        
        text = ""
        for line in haiku_array:            #For each haiku line in the array (always 3)
            line = ' '.join(line.split())       #Remove duplicate spaces
            text += "\n" + line.strip()         #Concatenate lines into a single string, add line breaks
        print("Markov haiku complete.\n")
        context.bot.send_message(chat_id=chat_id, text=text)
                
        
    def eight_ball(self, update: Update, context: CallbackContext):
        print("Generating markov 8-Ball...")
        chat_id = update.message.chat_id
        
        EightBallResponses = ["Absolutely","Without a doubt","Yes","It is known.","Most likely.","Yes you autistic fuck.","Probably","Signs point to yes","Certainly","No doubt about it","I don't know","Ask again later","meh","outlook not so good","No fuck off","no ur gae","lol no","Very doubtful.","Absolutely not."]
        
        randnum = random.randint(0,len(EightBallResponses))
        if randnum == len(EightBallResponses):
            self.markov_haiku(update, context)
        else:
            response = EightBallResponses[randnum]
            context.bot.send_message(chat_id=chat_id, text=response)

    def markov_clear(self, update: Update, context: CallbackContext):
        chat_id = update.message.chat_id
        self.markov_data[chat_id] = {}
        context.bot.send_message(chat_id=chat_id, text="Markov data cleared")

    def process_text(self, text, chat_id):
        print("Capturing: " + text)

        # space delimited message breakup
        words = text.split(' ')
        if words:
            self.increase_freq('CHAIN_START', words[0], chat_id)

        for i in range(0, len(words)):
            if (i + 1) >= len(words):
                self.increase_freq(words[i], 'CHAIN_END', chat_id)
            else:
                self.increase_freq(words[i], words[i+1], chat_id)

    def capture_msg(self, update: Update, context: CallbackContext):
        chat_id = update.message.chat_id
        text = update.message.text

        # Send an 8-Ball response if message starts with certain phrases.
        if text.lower().startswith("markov") or text.lower().startswith("hey markov") or text.lower().startswith("yo markov"):
            self.eight_ball(update, context)
            return

        self.process_text(text, chat_id)

        # choose whether to send random message or not
        select = random.randint(1,100)
        if select <= self.rand_response_freq:
            text = self.markov_msg(chat_id)
            if not text:
                context.bot.send_message(chat_id=chat_id, text="No Data!")
            elif text.startswith("STICKER:"):
                text = text[len("STICKER:"):]
                context.bot.send_sticker(chat_id=chat_id, sticker=text)
            else:
                context.bot.send_message(chat_id=chat_id, text=text)

    def increase_freq(self, lhs, rhs, chat_id):
        # get the data for this chat
        if chat_id not in self.markov_data:
            self.markov_data[chat_id] = {}
        mk_data = self.markov_data[chat_id]

        # ensure word has a frequency list
        if lhs not in mk_data:
            mk_data[lhs] = []

        # check if the word is already in the frequency list
        for i in range(len(mk_data[lhs])):
            if mk_data[lhs][i][0] == rhs:
                mk_data[lhs][i] = (rhs, mk_data[lhs][i][1] + 1)
                return

        # add it if the previous statements found nothing
        mk_data[lhs].append((rhs, 1))

    def capture_sticker(self, update: Update, context: CallbackContext):
        chat_id = update.message.chat_id
        sticker = update.message.sticker
        sticker_id = "STICKER:" + sticker.file_id
        self.increase_freq('CHAIN_START',sticker_id,chat_id)
        self.increase_freq(sticker_id, 'CHAIN_END', chat_id)
        
    def capture_image(self, update: Update, context: CallbackContext):
        chat_id = update.message.chat_id
        file_id = update.message.photo[-1].file_id
        file = context.bot.get_file(file_id)
        print("Downloading image to: " + self.markov_image_file)
        file.download(custom_path = self.markov_image_file)
        print("Downloading complete")
        ocr_text = self.run_ocr(update.message.caption)
        os.remove(self.markov_image_file)
        if len(ocr_text) == 0:
            print("No text found.")
        else:
            if self.markov_print_ocr:
                context.bot.send_message(chat_id=chat_id, text=ocr_text)
            self.process_text(ocr_text, chat_id)

    def run_ocr(self, caption):
        print("Running ocr")
        # load the image downloaded from Telegram and convert to grayscale
        image = cv2.imread(self.markov_image_file)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # write the grayscale image to disk as a temporary file so we can apply OCR to it
        filename = self.markov_image_file_grayscale
        cv2.imwrite(filename, gray)

        # load the grayscale image as a PIL/Pillow image, apply OCR, and then delete the temporary file
        lang_dict = {'afrikaans': 'afr', 'amharic': 'amh', 'arabic': 'ara', 'assamese': 'asm', 'azerbaijani': 'aze', 'azerbaijani cyrillic': 'aze_cyrl', 'belarusian': 'bel', 'bengali': 'ben', '': 'syr', 'bosnian': 'bos', 'breton': 'bre', 'bulgarian': 'bul', 'catalan': 'cat', 'chinese': 'chi_tra', 'chinese simplified': 'chi_sim', 'simplified chinese': 'chi_sim','chinese simplified vertical': 'chi_sim_vert', 'simplified chinese vertical': 'chi_sim_vert','chinese traditional': 'chi_tra', 'traditional chinese': 'chi_tra','chinese traditional vertical': 'chi_tra_vert', 'traditional chinese vertical': 'chi_tra_vert','corsican': 'cos', 'danish': 'dan', 'divehi': 'div', 'dzongkha': 'dzo', 'english': 'eng', 'esperanto': 'epo', 'estonian': 'est', 'faroese': 'fao', 'finnish': 'fin', 'french': 'fra','western frisian': 'fry', 'gaelic': 'gla', 'irish': 'gle', 'galician': 'glg', 'gujarati': 'guj', 'haitian': 'hat', 'hebrew': 'heb', 'hindi': 'hin', 'croatian': 'hrv', 'hungarian': 'hun', 'inuktitut': 'iku', 'indonesian': 'ind', 'javanese': 'jav', 'japanese': 'jpn', 'japanese vertical': 'jpn_vert', 'kannada': 'kan', 'kazakh': 'kaz', 'khmer': 'khm', 'kyrgyz': 'kir', 'korean': 'kor', 'korean vertical': 'kor_vert', 'lao': 'lao', 'latin': 'lat', 'latvian': 'lav', 'lithuanian': 'lit', 'luxembourgish': 'ltz', 'letzeburgesch': 'ltz', 'malayalam': 'mal', 'marathi': 'mar', 'maltese': 'mlt', 'mongolian': 'mon', 'nepali': 'nep', 'norwegian': 'nor', 'occitan': 'oci', 'oriya': 'ori', 'ossetian': 'osd', 'ossetic': 'osd', 'panjabi': 'pan', 'punjabi': 'pan', 'polish': 'pol', 'portuguese': 'por', 'pushto': 'pus', 'pashto': 'pus', 'quechua': 'que', 'russian': 'rus', 'sanskrit': 'san', 'sinhala': 'sin', 'sinhalese': 'sin', 'slovenian': 'slv', 'sindhi': 'snd', 'spanish': 'spa', 'serbian latin': 'srp_latn', 'serbian': 'srp', 'sundanese': 'sun', 'swahili': 'swa', 'swedish': 'swe', 'tamil': 'tam', 'tatar': 'tat', 'telugu': 'tel', 'tajik': 'tgk', 'tagalog': 'tgl', 'thai': 'tha', 'tigrinya': 'tir', 'tonga': 'ton', 'turkish': 'tur', 'uighur': 'uig', 'uyghur': 'uig', 'ukrainian': 'ukr', 'urdu': 'urd', 'uzbek cyrillic': 'uzb_cyrl', 'uzbek': 'uzb', 'vietnamese': 'vie', 'yiddish': 'yid', 'yoruba': 'yor'}
        translated = False
        
        try:
            caption_abbreviation = lang_dict[caption.lower()]
            text = pytesseract.image_to_string(Image.open(filename), lang=caption_abbreviation)
            translated = True
        except:
            text = pytesseract.image_to_string(Image.open(filename))
            
        if translation_enabled and translated:
            translator = google_translator()
            translation = translator.translate(text, lang_tgt='en')
            return translation
            
        os.remove(filename)
        
        # return OCR text. Uncomment to return text with line breaks removed
        return text#.replace('\n', ' ').replace('\r', '')
